// 3
const getCube = (x) => x ** 3;
num = 3;
// 4
console.log(`The cube of ${num} is ${getCube(num)}`);
// 5
const address = ["258 Washington Ave NW", "California", "90011"];
// 6
const [street, country, postalCode] = address;
console.log(`I live at the ${street}, ${country} ${postalCode}`);
// 7
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	metricWeight: "1075 kgs",
	imperialSize: "20 ft 3 in"
}
// 8
const {name, species, metricWeight, imperialSize} = animal;
console.log(`${name} was a ${species}. He weighed at ${metricWeight} with a measurement of ${imperialSize}.`);
// 9
const numbers = [1, 2, 3, 4, 5];
// 10
numbers.forEach((i) => console.log(i));
// 11
class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
// 12
const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);